autopep8 (2.3.2-1) unstable; urgency=medium

  * New upstream release
  * watch: use github

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 17 Jan 2025 13:40:02 +0100

autopep8 (2.3.1-3) unstable; urgency=medium

  * Team upload.
  * Trim d/rules
  * Set DPT as Maintainer per new Team Policy
  * Drop dependency on python3-pep8: it was replaced by pycodestyle

 -- Alexandre Detiste <tchet@debian.org>  Tue, 14 Jan 2025 21:31:34 +0100

autopep8 (2.3.1-2) unstable; urgency=medium

  * Team upload.
  * Remove now-unused dependency on python3-pkg-resources (closes:
    #1083309).

 -- Colin Watson <cjwatson@debian.org>  Mon, 07 Oct 2024 13:13:33 +0100

autopep8 (2.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 06 Jul 2024 22:45:29 +0200

autopep8 (2.1.0-2) unstable; urgency=medium

  * Team Upload
  * Remove dependency on 2to3. (Closes: #1071692)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 14 Jun 2024 17:38:52 +0200

autopep8 (2.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 29 Mar 2024 16:30:21 +0100

autopep8 (2.0.4-2) unstable; urgency=medium

  * Team upload.
  * Patch: Python 3.12 support. (Closes: #1058198)

 -- Stefano Rivera <stefanor@debian.org>  Tue, 23 Jan 2024 17:23:35 -0400

autopep8 (2.0.4-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 30 Aug 2023 15:04:43 +0200

autopep8 (2.0.2-1) unstable; urgency=medium

  * Upload in unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 06 Jul 2023 21:48:37 +0200

autopep8 (2.0.2-1~exp1) experimental; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 10 Jun 2023 13:37:45 +0200

autopep8 (2.0.1-1) unstable; urgency=medium

  * New upstream release
    - debian/patches/lower-pycodestyle.diff (no longer needed)
  * Bump debhelper from old 12 to 13.
  * Update SV to 4.6.2

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 02 Nov 2022 23:31:23 -0000

autopep8 (1.7.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 11 Aug 2022 15:37:48 +0200

autopep8 (1.6.0-1) unstable; urgency=medium

  * New upstream release
  * Fix 'pypi-homepage'

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 07 Nov 2021 12:15:44 +0100

autopep8 (1.5.7-1) unstable; urgency=medium

  * Upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 21 Aug 2021 20:12:36 +0200

autopep8 (1.5.7-1~exp1) experimental; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 03 May 2021 21:38:17 +0200

autopep8 (1.5.6-1~exp1) experimental; urgency=medium

  * New upstream release
  * Update of the watch file

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 22 Mar 2021 23:20:45 +0100

autopep8 (1.5.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Sylvestre Ledru ]
  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 04 Feb 2021 11:16:29 +0100

autopep8 (1.5.4-1) unstable; urgency=low

  * New upstream release

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 10 Aug 2020 10:35:28 +0200

autopep8 (1.5.3-2) unstable; urgency=medium

  * Upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 16 Jun 2020 10:04:40 +0200

autopep8 (1.5.3-1) experimental; urgency=medium

  * New upstream release
    python3-toml is now a build dep

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 15 Jun 2020 22:12:58 +0200

autopep8 (1.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 10 May 2020 13:50:35 +0200

autopep8 (1.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 11 Apr 2020 13:03:05 +0200

autopep8 (1.5-1) unstable; urgency=medium

  * New upstream release
  * Update the dependency to pycodestyle the py3 version
    (Closes: #936169)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 22 Jan 2020 14:30:46 +0100

autopep8 (1.4.4-3) unstable; urgency=medium

  * Add missing breaks/replaces (Closes: #945950)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 01 Dec 2019 19:10:50 +0100

autopep8 (1.4.4-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove ancient X-Python3-Version field.

  [ Nicholas D Steeves ]
  * Drop bin:python-autopep8, its Build-Depends, and all Python 2
    references in rules. (Closes: #936169)
  * rules: Allow bin:python3-autopep8 to install to /usr/bin.  This
    bin:package now provides /usr/bin/autopep8. (Closes: #909812)
  * Drop Recommends: python-autopep8 from bin:python3-autopep8.
    (Closes: #939051)
  * Drop python3-autopep8.lintian-overrides, which the previous two
    actions make unnecessary.
  * Move python-autopep8.manpages to python3-autopep8.manpages.
  * Switch buildsystem from python_distutils to pybuild.  The package
    FTBFS without this, because of missing X-Python3-Version or
    debian/pyversions, and in the long-term the switch to pybuild is
    probably necessary.  Also, drop the overrides in rules that are
    obsolesced by this action.
  * Add build-dep on dh-sequence-python3.
  * Add python3-lib2to3 as a dependency for bin:python3-autopep8.  I'm not
    sure why it wasn't autodetected, but /usr/bin/autopep8 fails without
    this.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:35:32 +0200

autopep8 (1.4.4-1) unstable; urgency=medium

  * Upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 07 Jul 2019 09:47:05 +0200

autopep8 (1.4.4-1~exp1) experimental; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 20 Apr 2019 20:31:52 +0200

autopep8 (1.4.3-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 15 Nov 2018 13:59:04 +0100

autopep8 (1.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 26 Oct 2018 09:39:08 +0200

autopep8 (1.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 19 Oct 2018 14:31:09 +0200

autopep8 (1.4-1) unstable; urgency=medium

  * new upstream release
  * Standards-Version: 4.2.1

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 08 Sep 2018 11:58:59 +0200

autopep8 (1.3.5-2) unstable; urgency=medium

  * Created the new package python3-autopep8. Closes: #875906

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 15 May 2018 18:18:28 +0200

autopep8 (1.3.5-1) unstable; urgency=medium

  * New upstream release

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 08 Apr 2018 17:46:11 +0200

autopep8 (1.3.4-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 01 Feb 2018 20:31:10 +0100

autopep8 (1.3.3-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.1.1

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 24 Oct 2017 15:21:00 +0200

autopep8 (1.3.2-1) unstable; urgency=medium

  * Upload in unstable
  * Standards-Version updated to 4.1.0
  * Make the build reproducible
    Thanks to Chris Lamb for the patch (Closes: #870131

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 29 Aug 2017 09:14:17 +0200

autopep8 (1.3.2-1~exp1) experimental; urgency=medium

  * New upstream release (Closes: #806790)
  * Repo moved to git with the NMU (Closes: #848273)
  * Moved to the python team umbrella and me as maintainer
  * Update the min dep of pycodestyle
  * Vcs-Git & Vcs-Browser
  * Standards-Version updated 4.0.0
  * Add a manpage (Closes: #864167)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 19 Jun 2017 22:33:35 +0200

autopep8 (0.9.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Bumped DH level to 10.
  * debian/control:
      - Added the python-pkg-resources to Depends field. Thanks to
        Sebastian Ramacher. (Closes: #843977)
      - Added the python-pep8 and removed the pep8 from Depends field.
        Thanks to Antonio Ospite and Merlijn van Deen.
        (Closes: #817860, LP: #1635192)
      - Bumped Standards-Version to 3.9.8.
  * debian/copyright: removed a duplicate 'Copyright' word.
  * debian/watch: created.

 -- Daniele Adriana Goulart Lopes <danydrik@yahoo.com.br>  Wed, 14 Dec 2016 16:04:25 -0200

autopep8 (0.9.1-2) unstable; urgency=low

  * Update package short description to be correct (Closes: #711960)
  * Set version depends on pep8 (Closes: #712084)

 -- Micah Anderson <micah@debian.org>  Wed, 12 Jun 2013 16:46:32 -0400

autopep8 (0.9.1-1) unstable; urgency=low

  * Upgrade to new version (Closes: #710681)
  * Fix copyright

 -- Micah Anderson <micah@debian.org>  Tue, 11 Jun 2013 09:11:44 -0400

autopep8 (0.8.7-2) unstable; urgency=low

  * Remove full GPLv3 text from debian/copyright, point to common-licenses

 -- Micah Anderson <micah@debian.org>  Thu, 09 May 2013 15:32:52 -0400

autopep8 (0.8.7-1) unstable; urgency=low

  * Initial package (Closes: #707615)

 -- Micah Anderson <micah@debian.org>  Wed, 08 May 2013 14:35:49 -0400
